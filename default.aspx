﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="NewsWebsite._default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h3>Weekly News</h3>
        <asp:EntityDataSource ID="entityNews" runat="server" 
            ConnectionString="name=tradersDbEntities" 
            DefaultContainerName="tradersDbEntities" EnableFlattening="False" 
            EntitySetName="News">
        </asp:EntityDataSource>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="news_id" DataSourceID="entityNews">
            <Columns>
                <asp:BoundField DataField="date" HeaderText="date" SortExpression="date" />
                <asp:BoundField DataField="time" HeaderText="time" SortExpression="time" />
                <asp:BoundField DataField="timezone" HeaderText="timezone" 
                    SortExpression="timezone" />
                <asp:BoundField DataField="currency" HeaderText="currency" 
                    SortExpression="currency" />
                <asp:BoundField DataField="event" HeaderText="event" SortExpression="event" />
                <asp:BoundField DataField="importance" HeaderText="importance" 
                    SortExpression="importance" />
                <asp:BoundField DataField="actual" HeaderText="actual" 
                    SortExpression="actual" />
                <asp:BoundField DataField="forecast" HeaderText="forecast" 
                    SortExpression="forecast" />
                <asp:BoundField DataField="previous" HeaderText="previous" 
                    SortExpression="previous" />
                <asp:TemplateField HeaderText="weekdate" SortExpression="weekdate">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("weekdate") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("weekdate", "{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <h3>News Chart</h3>
        <asp:EntityDataSource ID="entityNewsChart" runat="server" 
            ConnectionString="name=tradersDbEntities" 
            DefaultContainerName="tradersDbEntities" EnableFlattening="False" 
            EntitySetName="NewsCharts" Include="NadexCurrency">
        </asp:EntityDataSource>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="news_id" DataSourceID="entityNewsChart">
            <Columns>
                <asp:TemplateField HeaderText="Currency">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("NadexCurreny.name") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("NadexCurrency.name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="sunday" HeaderText="sunday" 
                    SortExpression="sunday" />
                <asp:BoundField DataField="monday" HeaderText="monday" 
                    SortExpression="monday" />
                <asp:BoundField DataField="tuesday" HeaderText="tuesday" 
                    SortExpression="tuesday" />
                <asp:BoundField DataField="wednesday" HeaderText="wednesday" 
                    SortExpression="wednesday" />
                <asp:BoundField DataField="thursday" HeaderText="thursday" 
                    SortExpression="thursday" />
                <asp:BoundField DataField="friday" HeaderText="friday" 
                    SortExpression="friday" />
                <asp:TemplateField HeaderText="date" SortExpression="date">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("date","{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
